# Laporan Penjelasan dan Penyelesaian Soal

## Kelompok A03
| Nama                      | NRP            |
|---------------------------|----------------|
|James Silaban                                |5025201169  |
|Hemakesha Ramadhani Heriqbaldi               |5025201209  |
|Wina Tungmiharja                             |5025201242  |

# Soal 1

## a. Register.sh

**Membuat fungsi `make_directory`**

Setiap user yang berhasil mendaftar kana disimpan di file ./users/user.txt. Untuk membuat folder dan file tersebut, kita menggunakan fungsi `make_director()`

```
function make_directory(){
    if ! [ -d "users" ]
    then
        mkdir users
    fi

    if ! [ -f "users/user.txt" ]
    then
        touch users/user.txt
    fi
}
```
- penjelasan:
command `-d` merujuk pada direktori, command `-f` merujuk pada file, dan command `touch` digunakan untuk membuat file baru.

- eksekusi: 

```
if ! [ -d "users" ]
    then
        mkdir users
    fi
```
dicek apakah di direktori aktif saat ini terdapat direktori `users`, jika tidak ada, maka dibuat direktori baru.

```
if ! [ -f "users/user.txt" ]
    then
        touch users/user.txt
    fi
```
dicek apakah di direktori `users` terdapat file `user.txt`, jika tidak ada, maka file baru akan dibuat.

Fungsi `make_directory()` akan dipanggil pada pada fungsi `main()`.
```
main(){
    make_directory
    ...
}
```

**Membuat fungsi `validate_username()`**

Fungsi `validate_username()` digunakan untuk mengecek apakah username yang dimasukkan sudah terdaftar sebelumnya.

```
function validate_username(){
    local user_num=$(awk '$1==temp {++n} END {print n}' temp="$1" users/user.txt)
    if [ -z "$user_num" ]
    then
        user_num=0
        return 0
    else
        if [ $user_num -gt 0 ]
        then
        printf "\xE2\x9C\x96 User sudah ada\n"
        printf "%(%m/%d/%y %H:%M:%S)T REGISTER: ERROR User already exists\n" >> log.txt
        return 1 
        fi
    fi
}
```
- eksekusi:
```
local user_num=$(awk '$1==temp {++n} END {print n}' temp="$1" users/user.txt)
```
membaca file `user.txt` kemudian mengecek kesamaan username yang diinputkan dengan username yang ada pada file `user.txt` lalu dihitung banyak kesamaannya. Banyak kesamaannya akan dimasukkan pada variabel `user_num`.

```
if [ -z "$user_num" ]
    then
        user_num=0
        return 0
    else
        if [ $user_num -gt 0 ]
        then
        printf "\xE2\x9C\x96 User sudah ada\n"
        printf "%(%m/%d/%y %H:%M:%S)T REGISTER: ERROR User already exists\n" >> log.txt
        return 1 
        fi
    fi
```
dicek isi `user_num`. Jika variabel `user_num` = 0, berarti username yang diinputkan belum terdaftar dan bisa digunakan kemudian akan di return 0 menandakan nilai true. Tetapi jika variabel `user_num` lebih besar dari 0, maka akan dikeluarkan keterangan `User sudah ada` dan dikirim pesan kepada file `log.txt` dengan isi pesan `MM/DD/YY hh:mm:ss REGISTER: ERROR User already exists`.

Fungsi `validate_username()` akan dipanggil pada fungsi `main()`
```
main(){
    make_directory
    echo -n "Masukkan username : "
    read username

    while ! validate_username $username;
    do
        echo -n "Masukkan username : "
        read username
    done
    ...
}
```
Fungsi `validate_username()` digunakan sebagai kondisi pada perulangan. Jika fungsi `validate_username()` mengembalikan nilai 0 , maka username yang diinputkan dapat didaftarkan, tetapi jika sebaliknya, akan diminta inputan username kembali hingga username memenuhi persyaratan.

**Membuat fungsi `validate_password()`**

Fungsi `validate_password()` digunakan untuk mengecek apakah password yang diinputkan memenuhi persyaratan atau tidak.

```
function validate_password(){
    printf "\n"    
    if check_length $1 && check_character $1 && check_numeric $1 && check_unique $1
    then
        printf "\xE2\x9C\x94 Password memenuhi syarat"
        return 0
    else
        printf "\xE2\x9C\x96 Masukkan Password kembali\n"
        return 1 
    fi
}
```
- eksekusi:
Akan dilakukan percabangan untuk mengecek password dengan empat fungsi tambahan, yaitu `check_length()`, `check_character()`, `check_numeric()`, dan `check_unique()`.

    -  Fungsi `check_length()`

    ```
    function check_length(){
    if [[ ${#1} -ge 8 ]]
    then 
        printf "\xE2\x9C\x94 Password terdiri lebih dari 8 karakter\n"
        return 0 
    else 
        printf "\xE2\x9C\x96 Password belum terdiri setidaknya 8 karakter\n"
        return 1 
    fi
    }
    ```
    Jika password lebih dari delapan karakter, maka dikeluarkan keterangan `Password terdiri lebih dari 8 karakter` dan dikembalikan nilai 0. Jika sebaliknya, maka dikeluarkan keterangan `Password belum terdiri setidaknya 8 karakter` dan direturn nilai 1.
    
    - Fungsi `check_charackter()`
    ```
    function check_character(){
    if [[ "$1" == *[[:upper:]]* && "$1" == *[[:lower:]]* ]]
    then 
        printf "\xE2\x9C\x94 Password terdiri atas huruf kapital dan huruf kecil\n"
        return 0 
    else 
        printf "\xE2\x9C\x96 Password belum terdiri setidaknya 1 huruf kapital dan 1 huruf kecil\n"
        return 1 
    fi
    }
    ```
    Fungsi ini akan mengecek apakah password yang diinputkan terdapat minimal satu huruf kapital (`"$1" == *[[:upper:]]*`) dan satu huruf kecil (`"$1" == *[[:lower:]]*`). Jika kondisi memenuhi, maka dikeluarkan keterangan `Password terdiri atas huruf kapital dan huruf kecil` dan dikembalikan nilai 0. Sedangkan jika kondisi tidak terpenuhi, akan dikeluarkan keterangan `Password belum terdiri setidaknya 1 huruf kapital dan 1 huruf kecil` dan dikembalikan nilai 1.

    - Fungsi `check_numeric()`
    ```
    function check_numeric(){
    # Alphanumeric
    if [[ "$1" == *[0-9]* ]]
    then 
        printf "\xE2\x9C\x94 Password Alphanumeric\n"
        return 0 
    else 
        printf "\xE2\x9C\x96 Password belum terdiri setidaknya 1 angka\n"
        return 1 
    fi
    }
    ```
    Jika password yang diinputkan mengandung angka (`"$1" == *[0-9]*` bernilai benar), maka akan dikeluarkan keterangan `Password Alphanumeric` dan dikembalikan nilai 0. Jika sebaliknya, dikeluarkan keterangan `Password belum terdiri setidaknya 1 angka` dan dikembalikan nilai 1.

    - Fungsi `check_unique()`
    ```
    function check_unique(){
    # Tidak boleh sama dengan username
    if [[ !("$password" = "$username") ]]
    then 
        printf "\xE2\x9C\x94 Password Unik\n"
        return 0 
    else 
        printf "\xE2\x9C\x96 Password tidak boleh sama dengan username\n"
        return 1 
    fi}
    ```
    Jika password berbeda dari username (`!("$password" = "$username")` bernilai benar), maka akan dikeluarkan keterangan `Password Unik` dan dikembalikan nilai 0. Jika sebaliknya, dikeluarkan keterangan `Password tidak boleh sama dengan username` dan dikembalikan nilai 1.

Jika keseluruhan fungsi tambahan mengembalikan nilai 0, maka password memenuhi syarat dan dikeluarkan keterangan `Password memenuhi syarat` dan fungsi `validate_password()` akan mengembalikan  nilai 0. Sedangkan jika terdapat fungsi tambahan yang mengembalikan nilai 1, maka password tidak memenuhi syarat dan dikerluarkan keterangan `Masukkan Password kembali` dan mengembalikan nilai 1.

Fungsi `validate_password()` akan dipanggil pada fungsi `main()`
```
main(){
    make_directory
    echo -n "Masukkan username : "
    read username

    while ! validate_username $username;
    do
        echo -n "Masukkan username : "
        read username
    done

    echo -n "Masukkan password : "
    read -s password

    while ! validate_password $password;
    do
        echo -n "Masukkan password : "
        read -s password
    done
    ...
}
```
Setelah password diinput, maka selanjutnya yang akan dieksekusi adalah perulangan. Fungsi `validate_password()` digunakan sebagai kondisi untuk perulangan. Jika `validate_password()` mengembalikan nilai 1, maka password belum memenuhi syarat dan diminta untuk menginput kembali password.

**Memasukkan username dan password yang didaftar**

```
printf "$username $password\n" >> ./users/user.txt
```
Setelah username dan password yang diinput memenuhi syarat, maka username dan password tersebtu akan dimasukkan ke dalam file `user.txt`.

```
printf "%(%m/%d/%y %H:%M:%S)T REGISTER: INFO User "$username" registered successfully\n" >> log.txt

printf "\n\xE2\x9C\x94 Sukses Registrasi!\n"

```
Kemudian, keluarkan keterangan dengan format `MM/DD/YY hh:mm:ss REGISTER:
INFO User USERNAME registered successfully`.

```
main(){
    make_directory
    echo -n "Masukkan username : "
    read username

    while ! validate_username $username;
    do
        echo -n "Masukkan username : "
        read username
    done

    echo -n "Masukkan password : "
    read -s password

    while ! validate_password $password;
    do
        echo -n "Masukkan password : "
        read -s password
    done

    printf "$username $password\n" >> ./users/user.txt
    printf "%(%m/%d/%y %H:%M:%S)T REGISTER: INFO User "$username" registered successfully\n" >> log.txt
    printf "\n\xE2\x9C\x94 Sukses Registrasi!\n"
}
```
Perintah-perintah tersebut ditambahkan dalam fungsi `main()`.


## b. Main.sh

### b.1. Login
*Membuat fungsi `login()`*
```
function login(){
    echo -n "Masukkan username : "
    read username

    while ! find_username;
    do
        printf "\xE2\x9C\x96 Username belum terdaftar\n"
        echo -n "Masukkan username : "
        read username
    done

    echo -n "Masukkan password : "
    read -s password

    while ! find_data
    do
        printf "%(%m/%d/%y %H:%M:%S)T LOGIN: ERROR Failed login attempt on user $username\n" >> log.txt
        printf "\xE2\x9C\x96 Password salah\n"
        echo -n "Masukkan password : "
        read password
    done

    printf "\xE2\x9C\x94 Sukses Login\n"
    printf "%(%m/%d/%y %H:%M:%S)T LOGIN: INFO User $username logged in\n" >> log.txt
}
```
Fungsi `login()` digunakan untuk mengecek apakah user telah terdaftar atau tidak. Didalam fungsi `login()`, terdapat juga beberapa fungsi tambahan yang dibutuhkan, yaitu `find_username()` dan `find_data()`.

```
echo -n "Masukkan username : "
read username
```

Pada `login()`, user akan diminta menginputkan username.

```
while ! find_username;
do
    printf "\xE2\x9C\x96 Username belum terdaftar\n"
    echo -n "Masukkan username : "
    read username
done
```
Dicek apakah `username` yang diinputkan terdaftar atau tidak. Jika username belum terdaftar (`find_username()` mengembalikan nilai 1), lakukan perulangan untuk meminta input username hingga username yang dimasukkan terdaftar.

```
 echo -n "Masukkan password : "
read -s password
```
Jika `username` yang diinput terdaftar, kemudian minta kembali input password.

```
while ! find_data
do
    printf "%(%m/%d/%y %H:%M:%S)T LOGIN: ERROR Failed login attempt on user $username\n" >> log.txt
    printf "\xE2\x9C\x96 Password salah\n"
    echo -n "Masukkan password : "
    read password
done
```
Jika `password` tidak terdaftar (`find_data()` mengembalikan nilai 1), lakukan perulangan dengan mengeluarkan keterangan `MM/DD/YY hh:mm:ss LOGIN: ERROR Failed login attempt on user USERNAME` dan minta input password kembali.

```
printf "\xE2\x9C\x94 Sukses Login\n"
printf "%(%m/%d/%y %H:%M:%S)T LOGIN: INFO User $username logged in\n" >> log.txt
```
Jika `username` dan `password` yang diinputkan terdaftar, maka keluarkan keterangan `Sukses Login` dan kirim pesan `MM/DD/YY hh:mm:ss LOGIN: INFO User USERNAME logged in`

**Membuat fungsi `find_username()`**
```
function find_username(){
    local count=$(awk '$1==temp {++n} END {print n}' temp="$username" users/user.txt)
    if [ -z "$count" ]
    then
        count=0
        return 1
    else
        if [ $count -gt 0 ]
        then
            return 0 
        fi
    fi
}
```
penjelasan:  `-z` digunakan untuk mengecek variabel tersebut kosong (zero)

eksekusi: 
```
local count=$(awk '$1==temp {++n} END {print n}' temp="$username" users/user.txt)
```
File `user.txt` akan dibaca dan kemudian dicek kesamaan variabel `username` dengan argumen pertama di setiap baris pada file `user.txt`. Lalu dihitung banyak kesamaannya dan dimasukkan ke dalam variabel `count`.

```
if [ -z "$count" ]
then
    count=0
    return 1
else
    if [ $count -gt 0 ]
    then
        return 0 
    fi
fi
```
Dicek isi variabel `count`. Jika variabel tersebut tidak memiliki isi atau kosong, maka kembalikan nilai 1. Jika sebaliknya (nilai `count` lebih besar dari 0), kembalikan nilai 0.

**Membuat fungsi `find_data()`**
```
function find_data(){
    local string_data="$username $password"
    local checkPass=$(grep -w "$string_data" users/user.txt)
    if [ -z "$checkPass" ]
    then
        return 1
    else
        return 0
    fi
}
```
penjelasan : command `grep` digunakan untuk mencari pola pada suatu file dan menunjukkan hasilnya, dan command `-w` digunakan untuk menampilkan kesamaan pola untuk seluruh pola yang tersebut.

eksekusi:
```
local string_data="$username $password"
local checkPass=$(grep -w "$string_data" users/user.txt)
```
Mendeklarasikan variabel `string_data` dan nilai didalamnya adalah `username` `password`. Kemudian dilakukan pengecekan yang memiliki persamaan dengan variabel `string_data`. String yang sama akan dimasukkan ke dalam variabel `checkPass`

```
if [ -z "$checkPass" ]
then
    return 1
else
    return 0
fi
```
Jika variabel `checkPass` kosong, kembalikan nilai 1. Sedangkan jika ada isi variabel `checkPass`, kembalikan nilai 0.

```
main(){
    login    
    ...
}

```
Fungsi `login()` akan dipanggil pada fungsi `main()`


### b.2. Command

*Membuat fungsi `command`*
```
function command(){
    echo -n "Command Input : "
    read cmnd num

    if [ "$cmnd" = "att" ]
    then
        command_att 
    elif [ "$cmnd" = "dl" ]
    then
        command_dl
    fi
}
```
penjelasan :   <br />
- `-n` digunakan untuk mengecek apakah panjang string non zero 
- `cmnd` variabel yang digunakan untuk menuliskan command `att` atau `dl` 
- `num` variabel yang digunakan untuk menentukan jumlah foto yang akan didownload jika command berupa `dl` 
- `if [ "$cmnd" = "att" ]` digunakan jika value variabel cmnd berisi `att` 
- `command_att` berfungsi untuk memanggil fungsi `command_att` 
- `elif [ "$cmnd" = "dl" ]` digunakan jika value variabel cmnd berisi `dl` 
- `command_dl` berfungsi untuk memanggil fungsi `command_dl` 

eksekusi : <br />
- fungsi `command` akan dipanggil dari fungsi `main` setelah user login, didalam fungsi `command`, user dapat menginputkan dua jenis "command" yaitu `att` dan juga
`dl`. 
- jika user menginputkan command `att` maka fungsi akan memasukkan `att` kedalam variabel `cmnd`, dan di saat `if elif` akan dicek variabel tersebut dan akan memanggil fungsi `command_att`.
- jika user menginputkan command `dl` lalu menginputkan nomor untuk menentukan jumlah foto yang ingin di download, maka fungsi akan memasukkan `dl` kedalam variabel `cmnd` dan memasukkan nomor yang diinput kedalam variabel `num`, dan di saat `if elif` akan dicek variabel tersebut dan akan memanggil fungsi `command_dl`.

**Membuat fungsi `command_att`**
```
function command_att(){
    cat log.txt | awk -v user="$username" -F: '
    $0 ~ user{ 
        if($0 ~ "LOGIN:"){
            count++
        }
    }
     END {print count}'
}

```
penjelasan :  <br />
- `cat log.txt` digunakan untuk menjabarkan isi dari `log.txt`
- `| awk -v user="$username"` digunakan untuk mendefinisikan variabel, yaitu `user` dengan isinya sama dengan `$username`
- `-F:` digunakan untuk memsisahkan tiap kata dalam file dengan `:` 
- `$0 ~ user` digunakan untuk mencari kata yang sama dengan user ($username), saat ada yang sama, maka perintah `if` akan dieksekusi, hal ini diulang sampai kata yang sama dengan user ($username) habis di dalam `log.txt`
- `if($0 ~ "LOGIN:")` digunakan untuk mengecek jika ada kata `"LOGIN"` di dalam file `log.txt`
-`count++` digunakan sebagai penghitung, setiap kali ada kata `"LOGIN"` yang ditemukan di dalam file `log.txt`
- `END {print count}` Jika sudah selesai, maka akan meng-print atau meng-output kan nomor yang ada di dalam count  <br />

eksekusi : <br />
- fungsi `command_att` akan dipanggil dari fungsi `command` setelah user menginputkan `att` di dalam fungsi `command`
- fungsi dimulai dengan menjabarkan `log.txt` dengan menggunakan `cat`, lalu mendefinisikan variabel dengan isi nama username yang sedang login dengan cara `| awk -v user="$username"`. Setelah itu, setiap kata di file dipisahkan oleh `:` dengan cara `-F:`. lalu, `$0 ~ user` digunakan untuk mendeteksi kata yang sama dengan user ($username), saat ada yang sama, maka perintah `if` akan dieksekusi, parameter perintah if adalah `($0 ~ "LOGIN:")` yang digunakan untuk mengecek jika ada kata `"LOGIN"` di dalam file `log.txt`, jika ada, maka akan dieksekusi perintah `count++` yang akan menjumlahkan total kata LOGIN yang ada di dalam log.txt dan merupakan milik user yang sedang login. setelah proses selesai, maka akan ada `END {print count}` yang berfungsi untuk meng-print atau meng-output kan nomor yang ada di dalam count untuk menunjukkan berapa percobaan LOGIN yang berhasil maupun yang gagal.


**Membuat fungsi `command_dl`**
```
function command_dl(){ #command dl
    a=0 #variable loop
    nafolder='date +%Y-%m-%d' #bikin folder pake date dan masukin ke var. namafolder
    nafile='date +%Y-%m-%d' #bikin folder pake date dan masukin ke var. namafile
    namafolder=$(${nafolder}_$username) #bikin folder pake date dan masukin ke var. namafolder
    namafile=$(${nafile}_$username)
    if [[ -f "$namafile.zip" ]] #directory atau zip ada
    then 
        unzip -P $password $namafile.zip #unzip otomatis
        cntrfile=$(ls $namafolder | wc -l)
        blwlimit=$(expr $cntrfile + 1)
        abvlimit=$(expr $num + $cntrfile)
        cd $namafolder/ #ngubah directory ke dalam folder foto
        for a in $(seq -f "%02g" $blwlimit $abvlimit) #biar a punya "0X" didepannya
        do
            wget --output-document=PIC_$a.jpg https://loremflickr.com/320/240 #download
        done
        cd ..
        rm $namafile.zip
        zip -P $password -r $namafile.zip $namafolder  #zip balik otomatis
        rm -r $namafolder

    else #directory ngga ada
        mkdir $namafolder #bikin dir.
        cd $namafolder/ # change directory ke directory atas ^
        for a in $(seq -f "%02g" 1 $num) #biar a punya "0X" didepannya
        do
            wget --output-document=PIC_$a.jpg https://loremflickr.com/320/240
        done
        cd ..
        zip -P $password -r $namafile.zip $namafolder #zip otomatis
        rm -r $namafolder
     fi
}   

```

penjelasan :  <br />
- `a=0` variabel untuk `for loop`
- `nafolder='date +%Y-%m-%d'` digunakan untuk menuliskan tanggal dengan format YYYY-MM-DD untuk digabungkan dengan variabel `namafolder` saat penamaan folder
- `nafile='date +%Y-%m-%d'` digunakan untuk menuliskan tanggal dengan format YYYY-MM-DD untuk digabungkan dengan variabel `namafile` saat penamaan file
- `namafolder=$(${nafolder}_$username)` digunakan untuk menggabungkan tanggal dengan username yang sedang login untuk menamakan folder
-  `namafile=$(${nafile}_$username)` digunakan untuk menggabungkan tanggal dengan username yang sedang login untuk menamakan file
- ` if [[ -f "$namafile.zip" ]]` digunakan untuk mengecek jika file zip dengan nama yang sama di dalam directory ada atau tidak
- `unzip -P $password $namafile.zip` digunakan untuk meng-unzip secara otomatis dengan menginput password file zip secara otomatis dan meng-unzip file zip tersebut
- `cntrfile=$(ls $namafolder | wc -l)` digunakan untuk menghitung file yang ada di dalam directory (folder), digunakan untuk membuat parameter `for loop`
- `blwlimit=$(expr $cntrfile + 1)` digunakan untuk menentukan batas bawah `for loop` dengan cara menjumlahkan value variabel `cntrfile` dengan satu
- `abvlimit=$(expr $num + $cntrfile)` digunakan untuk menentukan batas atas `for loop` dengan cara menjumlahkan value variabel `num` dengan value variabel `cntrfile`
- `cd $namafolder/` digunakan untuk mengubah directory ke dalam folder yang akan diisi foto
- `for a in $(seq -f "%02g" $blwlimit $abvlimit)` loop yang digunakan untuk mendownload dan menamakan file foto, `"%02g"` digunakan untuk memberikan nilai 0 didepan `a` jika `a` merupakan satuan, `$blwlimit $abvlimit` parameter yang digunakan untuk mencegah penumpukan atau overwrite file di dalam folder
- `wget --output-document=PIC_$a.jpg https://loremflickr.com/320/240` digunakan untuk mendownload foto dari link yang diberikan dan dengan otomatis meng-output kan file dengan format `PIC_$a.jpg`
- `cd ..` digunakan untuk kembali ke directory induk dari `$namafolder`
- `rm $namafile.zip` digunakan untuk menghapus file zip yang lama
- `zip -P $password -r $namafile.zip $namafolder` digunakan untuk meng-zip folder `$namafolder` yang sudah dimasukkan oleh foto
- `rm -r $namafolder` digunakan untuk menghapus folder yang diisi foto, karena sudah ada versi zipnya
- `mkdir $namafolder` digunakan untuk membuat directory, karena directory dengan nama yang sesuai dengan `$namafolder` belum ada
- `for a in $(seq -f "%02g" 1 $num)` loop yang digunakan untuk mendownload dan menamakan file foto, `"%02g"` digunakan untuk memberikan nilai 0 didepan `a` jika `a` merupakan satuan, `1 $num` parameter yang digunakan untuk menghitung berapa foto yang akan didownload kedalam folder  <br />

eksekusi :  <br />
- fungsi `command_dl` akan dipanggil dari fungsi `command` setelah user menginputkan `dl` serta nomor untuk jumlah foto yang ingin di-download di dalam fungsi `command`
 ```
    nafolder='date +%Y-%m-%d' #bikin folder pake date dan masukin ke var. namafolder
    nafile='date +%Y-%m-%d' #bikin folder pake date dan masukin ke var. namafile
    namafolder=$(${nafolder}_$username) #bikin folder pake date dan masukin ke var. namafolder
    namafile=$(${nafile}_$username)
 ```
- keempat variabel digunakan untuk menamakan folder serta file zip. pertama `nafolder` dan `nafile` digunakan untuk memembuat tanggal sesuai format YYYY-MM-DD lalu `namafolder` dan `namafile` digunakan untuk menggabungkan valued  `nafolder` dan value `nafile` dengan value dari `username`, yang nantinya akan menjadi format YYYY-MM-DD_username (nama username disesuakan dengan yang sedang login saat ini)
```
if [[ -f "$namafile.zip" ]] #directory atau zip ada
    then 
```
- Jika file zip sebelumnya sudah ada dalam directory `soal1`, maka akan memenuhi prasyarat if dan menjalankan perintah didalam nya, perintah pertama adalah `unzip -P $password $namafile.zip #unzip otomatis` yang digunakan untuk meng-unzip file zip di dalam directory yang memiliki nama yang sama dengan `$namafile.zip`

```
    cntrfile=$(ls $namafolder | wc -l)
    blwlimit=$(expr $cntrfile + 1)
    abvlimit=$(expr $num + $cntrfile)
```
- setelah file zip sudah di unzip, `cntrfile` akan membuat list isi dari directory atau folder yang memiliki nama yang sama denga `$namafolder` dan menghitung jumlah isinya (jumlah line), lalu `blwlimit` akan menentukan batas bawah `for loop` dengan cara menjumlahkan value variabel `cntrfile` dengan satu, dan `abvlimit` akan menentukan batas atas `for loop` dengan cara menjumlahkan value variabel `num` dengan value variabel `cntrfile`

```
    cd $namafolder/ #ngubah directory ke dalam folder foto
    for a in $(seq -f "%02g" $blwlimit $abvlimit) #biar a punya "0X" didepannya
    do
        wget --output-document=PIC_$a.jpg https://loremflickr.com/320/240 #download
    done
    cd ..
```
- setelah mendapatkan parameter loop,`cd $namafolder/` digunakan untuk merubah directory ke dalam folder tempat foto di-download. Setelah itu diinisialisasikan  sebuah loop untuk mendownload file foto dan otomatis menamakannya dengan format nama `PIC_$a.jpg`, dengan `$a` dibatasi oleh `$blwlimit` sebagai batas bawah dan `$abvlimit` sebagai batas atas. `$a` juga dilengkapi dengan `"%02g"` yang berguna untuk menambahkan nilai 0 di depan `$a` jika `$a` merupakan satuan. setelah loop selesai, maka akan kembali ke directory induk atau parent dari folder tempat foto di-download menggunakan `cd ..`

```
    rm $namafile.zip
    zip -P $password -r $namafile.zip $namafolder  #zip balik otomatis
    rm -r $namafolder
```
- setelah sudah kembali ke directory induk dari folder tempat foto di-download, file zip lama dihapus dengan cara `rm $namafile.zip` karena sudah tidak dibutuhkan lagi, lalu folder yang sudah dimasukkan foto-foto akan di zip dengan cara `zip -P $password -r $namafile.zip $namafolder  #zip balik otomatis`, dan yang terakhir, menghapus folder tersebut karena sudah ada versi zip nya dengan menggunakan `rm -r $namafolder`

```
else #directory ngga ada
    mkdir $namafolder #bikin dir.
```
- jika file zip belum ada (tidak memenuhi prasyarat if), berarti menandakan bahwa belum ada directory yang dibuat untuk menyimpan foto, atau sudah ada, tetapi milik user yang lain atau memiliki tanggal pembuatan yang lain, karena tidak memenuhi prasayarat if maka akan menjalankan perintah di dalam else. Perintah pertama adalah membuat directory dengan nama yang berasal dari value variabel `namafolder`

```
    cd $namafolder/ # change directory ke directory atas ^
    for a in $(seq -f "%02g" 1 $num) #biar a punya "0X" didepannya
    do
        wget --output-document=PIC_$a.jpg https://loremflickr.com/320/240
    done
    cd ..
```
- setelah folder dibuat, `cd $namafolder/` digunakan untuk merubah directory ke dalam folder tempat foto di-download. Setelah itu diinisialisasikan  sebuah loop untuk mendownload file foto dan otomatis menamakannya dengan format nama `PIC_$a.jpg`, dengan `$a` dibatasi oleh 1 yang diinput oleh user sebagai batas bawah dan `$num` yang diinput oleh user sebagai batas atas. `$a` juga dilengkapi dengan `"%02g"` yang berguna untuk menambahkan nilai 0 di depan `$a` jika `$a` merupakan satuan. setelah loop selesai, maka akan kembali ke directory induk atau parent dari folder tempat foto di-download menggunakan `cd ..`

```
    zip -P $password -r $namafile.zip $namafolder #zip otomatis
    rm -r $namafolder
fi
```
- setelah berada di directory induk atau parent, maka folder yang ada di zip dengan cara `zip -P $password -r $namafile.zip $namafolder #zip otomatis`, setelah itu folder yang ada dihapus, karena sudah ada versi zip nya dengan menggunakan `rm -r $namafolder`

## Hasil yang didapatkan
- test case yang digunakan  :
```
Username 	: Jeremy
Password	: boss678AB

```
- Register menggunakan `register.sh`
![image](/uploads/40bbfffca96b5b6da80814e79a38be1f/image.png) 

- Hasil struktur folder setelah `register.sh` dijalankan  <br />
![image](/uploads/cc0b7b00189a2c80f399310582f37fd5/image.png) 

- Isi dari `user.txt` yang ada di dalam directory `users`
![image](/uploads/856a32a0d69a0879b4c1f3daaa67b28e/image.png) 

- Isi dari `log.txt`
![image](/uploads/2e9d6b9e7f0857f690409fde76aa598c/image.png)

- Download 1 foto menggunakan `command_dl` dengan cara memberi input `“dl 1”` di dalam `main.sh` setelah login
![image](/uploads/ffecb55ea37f15e7949599bfdf5d45a1/image.png)

- Hasil struktur folder setelah menjalankan `command_dl` <br />
![image](/uploads/82f0dbe7188ed4db87e13bb4670bd326/image.png)

- Hasil file zip di dalam `Files` Linux
![image](/uploads/4d789d02411f96cbb6d0e234fb3dcb32/image.png)

- Isi file zip
![image](/uploads/2a1abe6c313bd0d770dfa4e074397b74/image.png)

- Mengecek jumlah login menggunakan `command_att` dengan cara memberi input `”att”` di dalam `main.sh`
![image](/uploads/96b5ca51bb6c5759f54ff84e9c6c2ed8/image.png)

- Isi dari `log.txt`
![image](/uploads/5c5a98c721c6b0f4ad614ec667d1813c/image.png)

# Soal 2

## a. Membuat folder bernama forensic_log_website_daffainfo_log


**Membuat variabel bernama `log_directory` untuk menyimpan nama direktori**
```
log_directory="forensic_log_website_daffainfo_log"
```


**Membuat fungsi `make_directory`**
```
function make_directory(){
    #if log directory doesn't exist, do if
    if ! [ -d $1 ]
    then
        mkdir $1
    fi
}
```
- penjelasan :
`-d` digunakan untuk mengecek apakah suatu direktori exist 
`$1` digunakan untuk mengakses variabel pertama yang di pass ke _function_
`mkdir` digunakan untuk membuat direktori

- eksekusi :
ketika folder yang ingin kita buat belum ada, maka script akan langsung membuat folder tersebut


**Memanggil fungsi tersebut di `main()` untuk membuat direktori ketika program dijalankan**
```
main(){
    make_directory $log_directory
    ...
    ...
}
```


## b. Menghitung rata-rata request per jam yang dikirimkan penyerang ke website
kemudian menuliskan hasilnya ke `ratarata.txt` ke dalam folder yg telah dibuat


**Membuat fungsi `write_ratarata`**
```
function write_ratarata(){
    local total_request=$(cat log_website_daffainfo.log | wc -l)
    local request=$(echo "$total_request / 12" | bc -l)
    printf "Rata-rata serangan adalah sebanyak $request requests per jam" > $log_directory/ratarata.txt
}
```
- penjelasan :
`wc` adalah command yang digunakan untuk membaca baris, k=kata, atau karakter pada suatu file
`wc -l` digunakan untuk membaca banyaknya baris dalam suatu file

`bc` adalah scripting yang digunakan untuk memproses kalkulasi yang melibatkan presisi
`bc -l` digunakan untuk mengimport library mathlib dalam kalkulasi (menghindari adanya pembulatan)

- eksekusi :
`wc -l` akan digunakan untuk membaca banyaknya request yang dikirimkan dengan membaca berapa banyak baris pada file 
`log_website_daffainfo.log`, hasil yang diperoleh yaitu `973`. hasil tersebut akan disimpan dalam variabel lokal bernama `$total_request`.

Kemudian hasil tersebut dikalkulasikan untuk menghitung rerata request setiap jam, yaitu dengan rumus `total_request/12` menggunakan `bc -l` agar mendapat presisi yang diinginkan. Hasil tersebut kemudian disimpan di variabel `request`

setelah rerata didapatkan, maka hasil tersebut tinggal dituliskan ke `rerata.txt` dengan
```
printf "Rata-rata serangan adalah sebanyak $request requests per jam" > $log_directory/ratarata.txt
```


**Memanggil fungsi tersebut di `main()` untuk menjalankan perhitungan ketika program dijalankan**
```
main(){
    make_directory $log_directory
    write_ratarata
    ...
}
```


## C. Menampilkan IP yang paling banyak melakukan request ke server


**Membuat function  write_ip**
```
function write_ip(){
    cat log_website_daffainfo.log | awk -F: '
    { 
        if($1 !~ "IP"){
            count[$1]++
        } 
    }
	END {
        max=0
		ip_address
		for(i in count){
			if(max<count[i]){
                max=count[i]
				ip_address=i
			}
		}
		print "IP yang paling banyak mengakses server adalah: " ip_address " sebanyak " max " requests\n"
	}' >> $log_directory/result.txt
}
```
- eksekusi :


```
cat log_website_daffainfo.log | awk -F: 
```
melakukan baca file dan menjalankan script awk bersamaan
`-F` digunakan agar `awk` menggunakkan colon (:) sebagai pemisah setiap string yg diproses pada file


```
{ 
    if($1 !~ "IP"){
        count[$1]++
    } 
}
```
meng `exclude` baris pertama yaitu kaliamat dengan "IP" sebagai substring nya agar tidak kehitung
melakukan perhitungan munculnya setiap ip address unik dan disimpan dalam array `count[]`


```
END {
  max=0
  ip_address
  for(i in count){
    if(max<count[i]){
      max=count[i]
      ip_address=i
    }
  }
```
ketika array count sudah terisi dengan a banyak occurence untuk setiap ip adress unik, maka script akan ngeloop pada array tersebut dan menghitung ip adress mana yang paling banyak muncul, 
dilakukan secara linear dengan menggunakan variabl `max` yang akan diubah setiap kali ada `count[i]` (occurence ip adress) yang lebih banyak sambil menyimpan nilai ip address dalam variabel `ip_address`


```
print "IP yang paling banyak mengakses server adalah: " ip_address " sebanyak " max " requests\n"
	}' >> $log_directory/result.txt
```
menuliskan hasil `max` dan `ip_address` kedalam `result.txt`

**Memanggil fungsi tersebut di `main()` untuk menjalankan perhitungan ketika program dijalankan**
```
main(){
    make_directory $log_directory
    write_ratarata
    write_ip
    ...
}
```


## D. Menghitung banyak requests yang menggunakan user-agent curl

**Membuat function  write_curl**
```
function write_curl(){
    cat log_website_daffainfo.log | awk -F: '
    /curl/{ 
        count++
    }
	END {
		print "Ada " count " requests yang menggunakan curl sebagai user-agent\n"
	}' >> $log_directory/result.txt
}
```
- eksekusi :

```
cat log_website_daffainfo.log | awk -F: 
```
melakukan baca file dan menjalankan script awk bersamaan
`-F` digunakan agar `awk` menggunakkan colon (:) sebagai pemisah setiap string yg diproses pada file


```
/curl/{ 
    count++
}
```
melakukan _pattern matching_ degan regex `/curl/` untuk menemukan berapa benyak request yang menggunakan _user-agent_ _curl_ dan disimpan dalam variabel `count`


```
END {
    print "Ada " count " requests yang menggunakan curl sebagai user-agent\n"
}' >> $log_directory/result.txt
```
menuliskan hasil `count` dalam `result.txt`


**Memanggil fungsi tersebut di `main()` untuk menjalankan perhitungan ketika program dijalankan**
```
main(){
    make_directory $log_directory
    write_ratarata
    write_ip
    write_curl
    ...
}
```


## E. Mengcatat nama IP yang mengakses website pada jam 2 pagi

**Membuat function  write_attack**
```
function write_attack(){
    cat log_website_daffainfo.log | awk -F: '
    /22\/Jan\/2022:02:/{ 
        count[$1]++
    }
    END {
        for(i in count){
			print i
		}
	}' >> $log_directory/result.txt
}
```
- eksekusi :

```
cat log_website_daffainfo.log | awk -F: 
```
melakukan baca file dan menjalankan script awk bersamaan
`-F` digunakan agar `awk` menggunakkan colon (:) sebagai pemisah setiap string yg diproses pada file


```
/22\/Jan\/2022:02:/{ 
    count[$1]++
}
```
untuk setiap baris, melakukan _pattern matching_ degan regex `/22\/Jan\/2022:02:/` untuk menemukan berapa benyak request pada jam 2 pagi dan disimpan dalam array `count`


```
 END {
      for(i in count){
    print i
  }
}' >> $log_directory/result.txt
```
menuliskan nama-nama Ip adress dalam `result.txt`
    
    
**Memanggil fungsi tersebut di `main()` untuk menjalankan perhitungan ketika program dijalankan**
```
main(){
    make_directory $log_directory
    write_ratarata
    write_ip
    write_curl
    write_attack
}
```

## Hasil yang didapatkan
- struktur folder setelah script dijalankan

![image](/uploads/c47dd1232f4ef569def3ad8c9f7c591f/image.png)

- `result.txt`
![image](/uploads/09a81d8080e866199d6f9caabe13676e/image.png)

- `rerata.txt`
![image](/uploads/2c259f456ecd94f259181afc860a7dc5/image.png)


# Kendala
- Minimnya dokumentasi yang lengkap dan jelas membuat kami kesulitan untuk melakukan eksekusi command dengan cepat
- Masih banyak aturan-aturan bash yang belum dimengerti menambah kesulitan saat pengerjaan










