#!/bin/bash

log_directory="forensic_log_website_daffainfo_log"

function make_directory(){
    #if log directory doesn't exist, do if
    if ! [ -d $1 ]
    then
        mkdir $1
    fi
}

function write_ratarata(){
    local total_request=$(cat log_website_daffainfo.log | wc -l)
    local request=$(echo "$total_request / 12" | bc -l)
    printf "Rata-rata serangan adalah sebanyak $request requests per jam" > $log_directory/ratarata.txt
}

function write_ip(){
    cat log_website_daffainfo.log | awk -F: '
    { 
        if($1 !~ "IP"){
            count[$1]++
        } 
    }
	END {
        max=0
		ip_address
		for(i in count){
			if(max<count[i]){
                max=count[i]
				ip_address=i
			}
		}
		print "IP yang paling banyak mengakses server adalah: " ip_address " sebanyak " max " requests\n"
	}' >> $log_directory/result.txt
}

function write_curl(){
    cat log_website_daffainfo.log | awk -F: '
    /curl/{ 
        count++
    }
	END {
		print "Ada " count " requests yang menggunakan curl sebagai user-agent\n"
	}' >> $log_directory/result.txt
}

function write_attack(){
    cat log_website_daffainfo.log | awk -F: '
    /22\/Jan\/2022:02:/{ 
        count[$1]++
    }
    END {
        for(i in count){
			print i
		}
	}' >> $log_directory/result.txt
}

main(){
    make_directory $log_directory
    write_ratarata
    write_ip
    write_curl
    write_attack
}

if [[ "${#BASH_SOURCE[@]}" -eq 1 ]]; then
    main "$@"
fi