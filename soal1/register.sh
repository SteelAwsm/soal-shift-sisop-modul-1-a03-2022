#!/bin/bash

function check_character(){
    # Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    if [[ "$1" == *[[:upper:]]* && "$1" == *[[:lower:]]* ]]
    then 
        printf "\xE2\x9C\x94 Password terdiri atas huruf kapital dan huruf kecil\n"
        return 0 
    else 
        printf "\xE2\x9C\x96 Password belum terdiri setidaknya 1 huruf kapital dan 1 huruf kecil\n"
        return 1 
    fi
}

function check_length(){
    # Minimal 8 karakter
    if [[ ${#1} -ge 8 ]]
    then 
        printf "\xE2\x9C\x94 Password terdiri lebih dari 8 karakter\n"
        return 0 
    else 
        printf "\xE2\x9C\x96 Password belum terdiri setidaknya 8 karakter\n"
        return 1 
    fi
}

function check_numeric(){
    # Alphanumeric
    if [[ "$1" == *[0-9]* ]]
    then 
        printf "\xE2\x9C\x94 Password Alphanumeric\n"
        return 0 
    else 
        printf "\xE2\x9C\x96 Password belum terdiri setidaknya 1 angka\n"
        return 1 
    fi
}

function check_unique(){
    # Tidak boleh sama dengan username
    if [[ !("$password" = "$username") ]]
    then 
        printf "\xE2\x9C\x94 Password Unik\n"
        return 0 
    else 
        printf "\xE2\x9C\x96 Password tidak boleh sama dengan username\n"
        return 1 
    fi
}

function validate_password(){
    printf "\n"    
    if check_length $1 && check_character $1 && check_numeric $1 && check_unique $1
    then
        printf "\xE2\x9C\x94 Password memenuhi syarat"
        return 0
    else
        printf "\xE2\x9C\x96 Masukkan Password kembali\n"
        return 1 
    fi
}

function validate_username(){
    #mengecek apakah sudah ada username yang telah terdaftar
    local user_num=$(awk '$1==temp {++n} END {print n}' temp="$1" users/user.txt)
    #mengecek apakah variabel user_num kosong(zero) atau tidak
    if [ -z "$user_num" ]
    then
        user_num=0
        return 0
    else
        if [ $user_num -gt 0 ]
        then
        printf "\xE2\x9C\x96 User sudah ada\n"
        printf "%(%m/%d/%y %H:%M:%S)T REGISTER: ERROR User already exists\n" >> log.txt
        return 1 
        fi
    fi
}

function make_directory(){
    #if directory users doesn't exist, do if
    if ! [ -d "users" ]
    then
        mkdir users
    fi
    #if file users/user.txt doesn't exist, do if
    if ! [ -f "users/user.txt" ]
    then
        touch users/user.txt
    fi
}


main(){
    make_directory
    echo -n "Masukkan username : "
    read username

    while ! validate_username $username;
    do
        echo -n "Masukkan username : "
        read username
    done

    echo -n "Masukkan password : "
    read -s password

    while ! validate_password $password;
    do
        echo -n "Masukkan password : "
        read -s password
    done

    #input username dan password ke ./users/user.txt
    printf "$username $password\n" >> ./users/user.txt
    #masukkan keterangan pada log.txt
    printf "%(%m/%d/%y %H:%M:%S)T REGISTER: INFO User "$username" registered successfully\n" >> log.txt

    printf "\n\xE2\x9C\x94 Sukses Registrasi!\n"
}

if [[ "${#BASH_SOURCE[@]}" -eq 1 ]]; then
    main "$@"
fi