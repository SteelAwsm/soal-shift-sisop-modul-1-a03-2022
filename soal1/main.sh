#!/bin/bash

. ./register.sh --source-only


function find_data(){
    local string_data="$username $password"
    local checkPass=$(grep -w "$string_data" users/user.txt)
    if [ -z "$checkPass" ]
    then
        return 1
    else
        return 0
    fi
}

function find_username(){
    local count=$(awk '$1==temp {++n} END {print n}' temp="$username" users/user.txt)
    if [ -z "$count" ]
    then
        count=0
        return 1
    else
        if [ $count -gt 0 ]
        then
            return 0 
        fi
    fi
}

function command_att(){
    cat log.txt | awk -v user="$username" -F: '
    $0 ~ user{ 
        if($0 ~ "LOGIN:"){
            count++
        }
    }
     END {print count}'
}

function command_dl(){ #command dl
    a=0 #variable loop
    nafolder='date +%Y-%m-%d' #bikin folder pake date dan masukin ke var. namafolder
    nafile='date +%Y-%m-%d' #bikin folder pake date dan masukin ke var. namafile
    namafolder=$(${nafolder}_$username) #bikin folder pake date dan masukin ke var. namafolder
    namafile=$(${nafile}_$username)
    if [[ -f "$namafile.zip" ]] #directory atau zip ada
    then 
        unzip -P $password $namafile.zip #unzip otomatis
        cntrfile=$(ls $namafolder | wc -l)
        blwlimit=$(expr $cntrfile + 1)
        abvlimit=$(expr $num + $cntrfile)
        cd $namafolder/ #ngubah directory ke dalam folder foto
        for a in $(seq -f "%02g" $blwlimit $abvlimit) #biar a punya "0X" didepannya
        do
            wget --output-document=PIC_$a.jpg https://loremflickr.com/320/240 #download
        done
        cd ..
        rm $namafile.zip
        zip -P $password -r $namafile.zip $namafolder  #zip balik otomatis
        rm -r $namafolder

    else #directory ngga ada
        mkdir $namafolder #bikin dir.
        cd $namafolder/ # change directory ke directory atas ^
        for a in $(seq -f "%02g" 1 $num) #biar a punya "0X" didepannya
        do
            wget --output-document=PIC_$a.jpg https://loremflickr.com/320/240
        done
        cd ..
        zip -P $password -r $namafile.zip $namafolder #zip otomatis
        rm -r $namafolder
     fi
}   

function login(){
    echo -n "Masukkan username : "
    read username

    while ! find_username;
    do
        printf "\xE2\x9C\x96 Username belum terdaftar\n"
        echo -n "Masukkan username : "
        read username
    done

    echo -n "Masukkan password : "
    read -s password

    while ! find_data
    do
        printf "%(%m/%d/%y %H:%M:%S)T LOGIN: ERROR Failed login attempt on user $username\n" >> log.txt
        printf "\xE2\x9C\x96 Password salah\n"
        echo -n "Masukkan password : "
        read password
    done

    printf "\xE2\x9C\x94 Sukses Login\n"
    printf "%(%m/%d/%y %H:%M:%S)T LOGIN: INFO User $username logged in\n" >> log.txt
}

function command(){
    echo -n "Command Input : "
    read cmnd num

    if [ "$cmnd" = "att" ]
    then
        command_att 
    elif [ "$cmnd" = "dl" ]
    then
        command_dl
    fi
}

main(){
    login
    
    command
}

if [[ "${#BASH_SOURCE[@]}" -eq 1 ]]; then
    main "$@"
fi
